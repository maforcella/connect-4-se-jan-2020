Gitlab Page Link: https://maforcella.gitlab.io/connect-4-se-jan-2020

Assistance Credits:
    Kevin Clark
    Marcus Chiriboga
    Randy's Starter Code

Development Plan:
    Didn't really have one for this.  Just listened to Randy's demos, and read through his starter code line by line with help from peers listed above.  Read through other student development plans and tried to work on specific functions to get them working properly.  As of this point the only original code that I wrote is the switchToNextPlayer function, as well as some variable names reasigned to work with color names instead of numbers.  I also did some light editing of the css.  I have also begun writing the horizontal winner function.  Turning in what I have currenttly on the advice of Randy.  Will return to attempt further completion at a later date.